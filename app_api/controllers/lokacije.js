var mongoose = require('mongoose');
var Lokacija = mongoose.model('Lokacija');

var vrniJsonOdgovor = function(odgovor, status, vsebina) {
  odgovor.status(status);
  odgovor.json(vsebina);
};

module.exports.lokacijeSeznamPoRazdalji = function(zahteva, odgovor) {
  var lng = parseFloat(zahteva.query.lng);
  var lat = parseFloat(zahteva.query.lat);
  var razdalja = parseFloat(zahteva.query.maxRazdalja);
  razdalja = isNaN(razdalja) ? 20 : razdalja;
  var tocka = {
    type: "Point",
    coordinates: [lng, lat]
  };
  var geoParametri = {
    spherical: true,
    maxDistance: razdalja * 1000,
    num: 10
  };
  if ((!lng && lng !== 0) || (!lat && lat !== 0)) {
    vrniJsonOdgovor(odgovor, 404, { "sporočilo": "Parametra lng and lat sta obvezna." });
    return;
  }
  Lokacija.geoNear(tocka, geoParametri, function(napaka, rezultati) {
    var lokacije = [];
    if (napaka) {
      vrniJsonOdgovor(odgovor, 404, napaka);
    } else {
      rezultati.forEach(function(dokument) {
        lokacije.push({
          razdalja: dokument.dis / 1000,
          naziv: dokument.obj.naziv,
          naslov: dokument.obj.naslov,
          ocena: dokument.obj.ocena,
          lastnosti: dokument.obj.lastnosti,
          _id: dokument.obj._id
        });
      });
      vrniJsonOdgovor(odgovor, 200, lokacije);
    }
  });
};

module.exports.lokacijeKreiraj = function(zahteva, odgovor) {
  console.log(zahteva.body.zaprto1);
  Lokacija.create({
    naziv: zahteva.body.naziv,
    naslov: zahteva.body.naslov,
    lastnosti: zahteva.body.lastnosti.split(","),
    koordinate: [parseFloat(zahteva.body.lng), parseFloat(zahteva.body.lat)],
    delovniCas: [{
      dnevi: zahteva.body.dnevi1,
      odprtje: zahteva.body.odprtje1,
      zaprtje: zahteva.body.zaprtje1,
      zaprto: zahteva.body.zaprto1
    }, {
      dnevi: zahteva.body.dnevi2,
      odprtje: zahteva.body.odprtje2,
      zaprtje: zahteva.body.zaprtje2,
      zaprto: zahteva.body.zaprto2
    }]
  }, function(napaka, lokacija) {
    if (napaka)
      vrniJsonOdgovor(odgovor, 400, napaka);
    else
      vrniJsonOdgovor(odgovor, 201, lokacija);
  });
};

module.exports.lokacijePreberiIzbrano = function(zahteva, odgovor) {
  if (zahteva.params && zahteva.params.idLokacije) {
    Lokacija
      .findById(zahteva.params.idLokacije)
      .exec(function(napaka, lokacija) {
        if (!lokacija) {
          vrniJsonOdgovor(odgovor, 404, { "sporočilo": 
            "Ne najdem lokacije s podanim enoličnim identifikatorjem idLokacije." });
          return;
        } else if (napaka) {
          vrniJsonOdgovor(odgovor, 404, napaka);
          return;
        }
        vrniJsonOdgovor(odgovor, 200, lokacija);
      });    
  } else {
    vrniJsonOdgovor(odgovor, 404, { "sporočilo": 
      "Manjka enolični identifikator idLokacije"});
  }
};

module.exports.lokacijePosodobiIzbrano = function(zahteva, odgovor) {
  if (!zahteva.params.idLokacije) {
    vrniJsonOdgovor(odgovor, 404, {"sporočilo": "Ne najdem lokacije, idLokacije je obvezen parameter."});
    return;
  }
  var noveLastnosti = zahteva.body.lastnosti + ',' + zahteva.body.naziv;
  noveLastnosti = noveLastnosti.split(',');
  // Lokacija.findById(zahteva.params.idLokacije).select('delovniCas').exec(function(napaka, lokacija) {
  //   //console.log("LOKACIJCICI:", lokacija.delovniCas[0].zaprto);
  //   var novDel = lokacija.delovniCas;
  //   console.log(novDel);
  //   novDel[0].odprtje = zahteva.body.naziv;
  //   console.log(novDel);
    
  //   Lokacija.update({_id: zahteva.params.idLokacije}, {$set: {delovniCas: novDel}}, function(napaka) {
  //       if (napaka) {
  //         vrniJsonOdgovor(odgovor, 404, napaka);
  //       } else {
  //         var a = "a";
  //         vrniJsonOdgovor(odgovor, 200, "DELa");
  //       }
  //   })
  // });

  Lokacija.update({ _id: zahteva.params.idLokacije }, { $set: { naziv: zahteva.body.naziv, lastnosti: noveLastnosti }}, function (napaka) {
    if (napaka) {
      vrniJsonOdgovor(odgovor, 404, napaka);
    }
    else {
      vrniJsonOdgovor(odgovor, 200, "nekaj");
    }
  });
  // Lokacija
  //   .findById(zahteva.params.idLokacije)
  //   .select('-komentarji -ocena')
  //   .exec(
  //     function(napaka, lokacija) {
  //       if (!lokacija) {
  //         vrniJsonOdgovor(odgovor, 404, {"sporočilo": "Ne najdem lokacije."});
  //         return;
  //       } else if (napaka) {
  //         vrniJsonOdgovor(odgovor, 400, napaka);
  //         return;
  //       }
  //       lokacija.naziv = zahteva.body.naziv;
  //       console.log("NOV NAZIV:", lokacija.naziv);
  //       lokacija.naslov = zahteva.body.naslov;
  //       // console.log("LASTNOSTI V LOKACIJE.JS:", zahteva.body.lastnosti);
  //       lokacija.lastnosti = zahteva.body.lastnosti;
  //       // lokacija.koordinate = [parseFloat(zahteva.body.lng), parseFloat(zahteva.body.lat)];
  //       lokacija.koordinate = zahteva.body.koordinate;
  //       // lokacija.delovniCas = [{
  //       //   dnevi: zahteva.body.dnevi1,
  //       //   odprtje: zahteva.body.odprtje1,
  //       //   zaprtje: zahteva.body.zaprtje1,
  //       //   zaprto: zahteva.body.zaprto1
  //       // }, {
  //       //   dnevi: zahteva.body.dnevi2,
  //       //   odprtje: zahteva.body.odprtje2,
  //       //   zaprtje: zahteva.body.zaprtje2,
  //       //   zaprto: zahteva.body.zaprto2
  //       // }];
  //       lokacija.delovniCas = zahteva.body.delovniCas;
  //       // lokacija.save(function(napaka, lokacija) {
  //       //   if (napaka)
  //       //     vrniJsonOdgovor(odgovor, 404, napaka);
  //       //   else
  //       //     vrniJsonOdgovor(odgovor, 200, lokacija);
  //       // });
  //       Lokacija.update({ _id: id }, { $set: { size: 'large' }}, callback);
  //     }
  //   );
};

module.exports.lokacijeIzbrisiIzbrano = function(zahteva, odgovor) {
  var idLokacije = zahteva.params.idLokacije;
  if (idLokacije) {
    Lokacija
      .findByIdAndRemove(idLokacije)
      .exec(
        function(napaka, lokacija) {
          if (napaka) {
            vrniJsonOdgovor(odgovor, 404, napaka);
            return;
          }
          vrniJsonOdgovor(odgovor, 204, null);
        }
      );
  } else {
    vrniJsonOdgovor(odgovor, 404, {"sporočilo": "Ne najdem lokacije, idLokacije je obvezen parameter."});
  }
};