# Pisni izpit pri predmetu Spletno programiranje (SP) v študijskem letu 2017/2018

Repozitorij z demo aplikacijo **EduGeoCache** je del gradiva za pisni izpit pri predmetu **Spletno programiranje**, ki ga v študijskem letu 2017/2018 izvaja [doc. dr. Dejan Lavbič](http://www.lavbic.net/) na [Fakulteti za računalništvo in informatiko](http://www.fri.uni-lj.si/), [Univerze v Ljubljani](http://www.uni-lj.si/).

## Navodila za vzpostavitev delovnega okolja za pisni izpit

### Fork izvornega repozitorija

Na spletnem naslovu https://bitbucket.org/dlavbic/sp-2017-2018-izpit/ se nahaja izvorna verzija aplikacije **EduGeoCache**, ki jo morate dopolniti. Najprej boste izvedli `fork` omenjenega repozitorija tako, da obiščete spletno povezavo [https://bitbucket.org/dlavbic/sp-2017-2018-izpit/**fork**/](https://bitbucket.org/dlavbic/sp-2017-2018-izpit/fork).

Pri kreiranju lastne kopije repozitorija je potrebno nastaviti:

* **ime repozitorija** nastavite ne **`sp-izpit-x`**, kjer **`x`** predstavlja zaporedno številko izpita (npr. `sp-izpit-1`, `sp-izpit-2` oz. `sp-izpit-3`),
* repozitorij mora biti **privaten**.

![](docs/Bitbucket_fork.png)

### Dostop do privatnega repozitorija

Dostop do obstoječe lastne privatne kopije repozitorija morate dodati dostop uporabnikov **`dlavbic`** in
**`asistent`**.

![](docs/Bitbucket_access.png)

### Kreiranje Cloud9 delovnega okolja

Pri implementaciji zahtevanih funkcionalnosti v okviru pisnega izpita se priporoča uporaba razvojnega okolja [Cloud9](https://c9.io/).

Če v Cloud9 še nimate pripravljenega delovnega okolja (Workspace), potem:

* kreirajte **privatno** delovno **okolje `Private`**,
* nastavite ustrezno **ime** delovnega okolja (npr. `sp-izpit-x`),
* izberite **`Hosted workspace`**, kamor vnesete naslov repozitorija **`https://{vaš-Bitbucket-račun}@bitbucket.org/{vaš-Bitbucket-račun}/sp-izpit-{x}.git`** z izvorno kodo obstoječe EduGeoCache aplikacije, ki jo boste nadgradili in se nahaja v vašem lastnem repozitoriju,
* in izberite **prazno predlogo `Blank`**.

![](docs/Cloud9_create_workspace.png)

### Namestitev podatkovne baze MongoDB

Za namestitev zadnje verzije **podatkovne baze MongoDB** v terminalu izvedite spodnje ukaze.

```
sudo apt-get remove mongodb-org mongodb-org-server
sudo apt-get autoremove
sudo rm -rf /usr/bin/mongo*
echo "deb http://repo.mongodb.org/apt/ubuntu trusty/mongodb-org/3.4 multiverse" | sudo tee /etc/apt/sources.list.d/mongodb-org-3.4.list
sudo apt-get update
sudo apt-get install mongodb-org mongodb-org-server
sudo touch /etc/init.d/mongod
sudo apt-get install mongodb-org-server
```

Ko je namestitev končana, s spodnjimi ukazi **pripravimo mapo s podatki** in **zagonsko datoteko podatkovne baze**.

```
mkdir mongodb
cd mongodb
mkdir data
echo 'mongod --bind_ip=$IP --dbpath=data --nojournal --rest "$@"' > mongod
chmod a+x mongod
```

Sedaj lahko z ukazom `./mongodb` podatkovno bazo MongoDB poženemo.

### Uvoz testnih podatkov v MongoDB podatkovno bazo

V Cloud9 okolju odprite nov zavihek s terminalom (New Terminal) in poženite spodnji ukaz za uvoz testnih podatkov v podatkovno bazo.

```
mongoimport --db edugeocache --collection Lokacije --mode upsert --upsertFields naziv --jsonArray --file ./testni_podatki.json
```

### Dodajanje okoljskih spremenljivk

Naša aplikacija EduGeoCache ima v okoljski spremenljivki shranjeno geslo za šifriranje občutljivih podatkov, ki se naloži iz datoteke `.env` in ni vključena v izvorno kodo, zato jo moramo s spodnjimi ukazi dodati.

```
echo 'JWT_GESLO=bestOfLuck' > .env
```

### Nameščanje odvisnih knjižnic

S spodnjim ukazom boste namestili vse potrebne knjižnice, ki jih aplikacija EduGeoCache uporablja pri svojem delovanju.

```
npm install
```

### Namestitev programa `nodemon`

Za lažje delo pri razvoju aplikacije si namestite program `nodemon`.

```
npm install -g nodemon
```

### Zagon aplikacije

Sedaj lahko s spodnjim ukazom poženete aplikacijo in ji dodajate funkcionalnosti glede na zahteve pisnega izpita.

```
nodemon
```