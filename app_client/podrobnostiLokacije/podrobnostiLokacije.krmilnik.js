(function() {
  /* global angular */

  function podrobnostiLokacijeCtrl($routeParams, $location, $uibModal, edugeocachePodatki, avtentikacija) {
    var vm = this;
    vm.idLokacije = $routeParams.idLokacije;
    
    vm.jePrijavljen = avtentikacija.jePrijavljen();
    
    // console.log($routeParams.podatki);
    
    vm.urediLokacijo = function () {
      
      var idLokacije = vm.idLokacije;
      console.log("UREDI LOKACIJO");
      
      edugeocachePodatki.posodobiLokacijoZaId(idLokacije, {
        naziv: vm.novoImeLokacije,
        naslov: vm.podatki.lokacija.naslov,
        lastnosti: vm.podatki.lokacija.lastnosti,
        koordinate: vm.podatki.lokacija.koordinate,
        delovniCas: vm.podatki.lokacija.delovniCas
      }).then(
        function success(odgovor) {
          console.log(odgovor);
          // vm.podatki.lokacija.naziv = vm.novoImeLokacije;
          console.log("PREJ:", vm.podatki.lokacija.lastnosti);
          var noveLastnosti = vm.podatki.lokacija.lastnosti + ',' + vm.novoImeLokacije;
          noveLastnosti = noveLastnosti.split(',');
          vm.podatki.lokacija.lastnosti = noveLastnosti;
          console.log("POLE:", noveLastnosti);
        },
        function error(odgovor) {
          console.log(odgovor);
        }
      );
      return false;

    };

    
    vm.odstraniKomentarSSeznama = function (idKomentarja) {
      for (var kom = 0; kom < vm.podatki.lokacija.komentarji.length; kom ++) {
        if (vm.podatki.lokacija.komentarji[kom]._id === idKomentarja) {
          
          vm.podatki.lokacija.komentarji.splice(kom, 1);
        }
      }      
    };
    
    vm.izbrisiKomentar = function (komentar) {
      console.log(komentar._id);
      // var idKomentarja = komentar._id;
      var idLokacije = vm.idLokacije;
      var idKomentarja = komentar._id;
      
      edugeocachePodatki.izbrisiKomentarZaId(idLokacije, idKomentarja).then(
        function success(odgovor) {
          console.log("Uspesno brisanje komentarja.");
          console.log(vm.podatki.lokacija.komentarji);

          vm.odstraniKomentarSSeznama(idKomentarja);
          
          console.log("drugic:", vm.podatki.lokacija.komentarji);
      
        },
        function error(odgovor) {
          console.log(odgovor);
        }
      );
      return false;
    };
    
    
    vm.jeTrenutniUporabnik = function (komentar) {
      
        if (avtentikacija.jePrijavljen()) {
          var trenutniEmail = avtentikacija.trenutniUporabnik().elektronskiNaslov;
          var komentarEmail = komentar.elektronskiNaslov;
          // console.log(komentarEmail, trenutniEmail);
          if (komentarEmail === trenutniEmail) {
            return true;
          }
        }
        return false;
    };
    
    vm.prvotnaStran = $location.path();
    
    edugeocachePodatki.podrobnostiLokacijeZaId(vm.idLokacije).then(
      function success(odgovor) {
        vm.podatki = { lokacija: odgovor.data };
        vm.glavaStrani = {
          naslov: vm.podatki.lokacija.naziv
        };
      },
      function error(odgovor) {
        console.log(odgovor.e);
      }
    );
    
    vm.prikaziPojavnoOknoObrazca = function(komentar) {
      
      var posodobiKomentar = false;
      
      var besediloKomentarja, ocenaKomentarja, komentarId;
      
      if (komentar === undefined) {
        besediloKomentarja = "";
        ocenaKomentarja = "";
        komentarId = "";
        posodobiKomentar = false;
      }
      else {
        besediloKomentarja = komentar.besediloKomentarja;
        ocenaKomentarja = komentar.ocena;
        komentarId = komentar._id;
        posodobiKomentar = true;
      }
        
      var primerekModalnegaOkna = $uibModal.open({
        templateUrl: '/komentarModalnoOkno/komentarModalnoOkno.pogled.html',
        controller: 'komentarModalnoOkno',
        controllerAs: 'vm',
        
        resolve: {
          podrobnostiLokacije: function() {
            return {
              idLokacije: vm.idLokacije,
              nazivLokacije: vm.podatki.lokacija.naziv,
              vsebinaKomentarja: besediloKomentarja, // dodal jaz
              prejOcena: ocenaKomentarja,
              posodobiKomentar: posodobiKomentar,
              komentarId: komentarId,
              odstraniKomentarSSeznama: vm.odstraniKomentarSSeznama
            };
          }
        }
      });
      
      primerekModalnegaOkna.result.then(function(podatki) {
        if (typeof podatki != 'undefined')
          vm.podatki.lokacija.komentarji.push(podatki);
      }, function() {
        // Ulovi dogodek in ne naredi ničesar
      });
    };
  }
  podrobnostiLokacijeCtrl.$inject = ['$routeParams', '$location', '$uibModal', 'edugeocachePodatki', 'avtentikacija'];
  
  angular
    .module('edugeocache')
    .controller('podrobnostiLokacijeCtrl', podrobnostiLokacijeCtrl);
})();