(function() {
  /* global angular */
  
  var edugeocachePodatki = function($http, avtentikacija) {
    var koordinateTrenutneLokacije = function(lat, lng) {
      return $http.get('/api/lokacije?lng=' + lng + '&lat=' + lat + '&maxRazdalja=100000');
    };
    var podrobnostiLokacijeZaId = function(idLokacije) {
      return $http.get('/api/lokacije/' + idLokacije);
    };
    var dodajKomentarZaId = function(idLokacije, podatki) {
      console.log("dodajKomentarZaId", idLokacije, podatki)
      return $http.post('/api/lokacije/' + idLokacije + '/komentarji', podatki, {
        headers: {
          Authorization: 'Bearer ' + avtentikacija.vrniZeton()
        }
      });
    };
    var posodobiKomentarZaId = function(idLokacije, komentarId, podatki) {
      console.log("posodobiKomentarZaId", idLokacije, komentarId, podatki)
      
      return $http.put('/api/lokacije/' + idLokacije + '/komentarji' + '/' + komentarId, podatki, {
        headers: {
          Authorization: 'Bearer ' + avtentikacija.vrniZeton()
        }
      });
    };
    
    var posodobiLokacijoZaId = function(idLokacije, podatki) {
      console.log("POSODOBI LOKACIJO ZA ID");
      console.log('/api/lokacije/' + idLokacije);
      return $http.put('/api/lokacije/' + idLokacije, podatki, {
        headers: {
          Authorization: 'Bearer ' + avtentikacija.vrniZeton()
        }
      });
    };
    var izbrisiKomentarZaId = function(idLokacije, idKomentarja) {
      return $http.delete('/api/lokacije/' + idLokacije + '/komentarji' + '/' + idKomentarja, {
        headers: {
          Authorization: 'Bearer ' + avtentikacija.vrniZeton()
        }
      });
    };
    return {
      koordinateTrenutneLokacije: koordinateTrenutneLokacije,
      podrobnostiLokacijeZaId: podrobnostiLokacijeZaId,
      dodajKomentarZaId: dodajKomentarZaId,
      izbrisiKomentarZaId: izbrisiKomentarZaId,
      posodobiKomentarZaId: posodobiKomentarZaId,
      posodobiLokacijoZaId: posodobiLokacijoZaId
    };
  };
  edugeocachePodatki.$inject = ['$http', 'avtentikacija'];
  
  angular
    .module('edugeocache')
    .service('edugeocachePodatki', edugeocachePodatki);
})();